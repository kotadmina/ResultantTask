//
//  NetworkService.swift
//  ResultantTask
//
//  Created by Sher Locked on 26.06.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class NetworkService: NetworkServiceProtocol {
    
    private let urlString = "http://phisix-api3.appspot.com/stocks.json"
    
    func getCurrency(completion: @escaping ([Currency]?) -> Void) {
        guard let url = URL(string: urlString) else {
            completion(nil)
            return
        }
        
        Alamofire.request(url, method: .get).responseJSON { response in
            guard response.result.isSuccess,
                let data = response.data,
                let json = try? JSON(data: data) else {
                completion(nil)
                return
            }
            
            let stockArray = json["stock"].arrayValue
            let currencyArray = stockArray.map({ stockValue -> Currency in
                let name = stockValue["name"].stringValue
                let volume = stockValue["volume"].intValue
                let priceJSON = stockValue["price"]
                let amount = priceJSON["amount"].numberValue
                return Currency(name: name, volume: volume, amount: amount)
            })
            completion(currencyArray)
        }
        
    }
    
}
