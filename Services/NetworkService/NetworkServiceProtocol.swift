//
//  NetworkServiceProtocol.swift
//  ResultantTask
//
//  Created by Sher Locked on 26.06.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

protocol NetworkServiceProtocol {
    func getCurrency(completion: @escaping ([Currency]?) -> Void)
}
