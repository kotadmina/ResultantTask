//
//  Currency.swift
//  ResultantTask
//
//  Created by Sher Locked on 26.06.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

struct Currency {
    
    var name: String
    var volume: Int
    var amount: NSNumber
    
}
