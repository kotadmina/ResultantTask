//
//  CurrencyListAssembly.swift
//  ResultantTask
//
//  Created by Sher Locked on 26.06.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit
import Swinject

class CurrencyListAssembly: BaseAssembly {
    
    static func configure() {
        let container = defaultContainer()
        
        // Register interactor
        container.register(CurrencyListInteractorInput.self) { r in
            let interactor = CurrencyListInteractor()
            interactor.networkService = r.resolve(NetworkServiceProtocol.self)!
            return interactor
            }.inObjectScope(.transient)
        
        // Register view
        container.register(CurrencyListViewInput.self) { r in
            let storyboard = UIStoryboard(name: "CurrencyListView", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "CurrencyListView") as! CurrencyListView
            vc.output = r.resolve(CurrencyListViewOutput.self, argument: (vc as CurrencyListViewInput))!
            return vc
            }.inObjectScope(.transient)
        
        // Register presenter
        container.register(CurrencyListViewOutput.self) { (r: Resolver, view: CurrencyListViewInput) in
            let presenter = CurrencyListPresenter()
            presenter.interactor = r.resolve(CurrencyListInteractorInput.self)!
            presenter.view = view
            presenter.vmf = r.resolve(CurrencyListVMFProtocol.self)!
            return presenter
            }.inObjectScope(.transient)
        
        //Register VMF
        container.register(CurrencyListVMFProtocol.self) { _ in
            return CurrencyListVMF()
        }.inObjectScope(.transient)
        
    }
}
