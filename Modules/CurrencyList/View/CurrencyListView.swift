//
//  CurrencyListView.swift
//  ResultantTask
//
//  Created by Sher Locked on 26.06.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class CurrencyListView: UIViewController, CurrencyListViewInput {
    
    var output: CurrencyListViewOutput!
    
    @IBOutlet weak var tableView: UITableView!
    
    private var viewModels: [CurrencyCellViewModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNav()
        configureTable()
        output.viewDidLoad()
    }
    
    private func configureNav() {
        let button = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(refreshButtonPressed))
        self.navigationItem.rightBarButtonItem = button
    }
    
    private func configureTable() {
        tableView.delegate = self
        tableView.dataSource = self
        let nib = UINib(nibName: "CurrencyCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "CurrencyCell")
    }
    
    func updateTable(with viewModels: [CurrencyCellViewModel]) {
        self.viewModels = viewModels
        tableView.reloadData()
    }
    
    func setNavigationTitle(text: String) {
        self.title = text
    }

    
    @objc func refreshButtonPressed() {
        output.refreshPressed()
    }

    
}

extension CurrencyListView: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CurrencyCell.cellHeight
    }
    
}

extension CurrencyListView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let viewModel = viewModels[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "CurrencyCell") as! CurrencyCell
        cell.configure(with: viewModel)
        return cell
    }
}
