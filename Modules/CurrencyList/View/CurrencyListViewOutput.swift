//
//  CurrencyListViewOutput.swift
//  ResultantTask
//
//  Created by Sher Locked on 26.06.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

protocol CurrencyListViewOutput {
    
    func viewDidLoad()
    func refreshPressed()
}
