//
//  CurrencyListPresenter.swift
//  ResultantTask
//
//  Created by Sher Locked on 26.06.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

class CurrencyListPresenter: CurrencyListViewOutput {
    
    var interactor: CurrencyListInteractorInput!
    weak var view: CurrencyListViewInput!
    var vmf: CurrencyListVMFProtocol!
    
    var timer: DispatchSourceTimer?
    let timerQueue = DispatchQueue(label: "TimerQueue", qos: .background)
    
    func viewDidLoad() {
        loadCurrency()
    }
    
    func refreshPressed() {
        loadCurrency()
    }
    
    private func loadCurrency() {
        timer = DispatchSource.makeTimerSource(flags: .strict, queue: timerQueue)
        timer?.schedule(deadline: .now(), repeating: .seconds(15))
        timer?.setEventHandler(handler: {
            DispatchQueue.main.async {
                self.view.setNavigationTitle(text: "Loading...")
            }
            self.interactor.getCurrency { currencyArray in
                let viewModels = self.vmf.createViewModels(with: currencyArray ?? [])
                DispatchQueue.main.async {
                    self.view.updateTable(with: viewModels)
                    self.view.setNavigationTitle(text: "")
                }
            }
        })
        timer?.resume()
    }
    
}
