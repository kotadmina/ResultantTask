//
//  CurrencyListVMF.swift
//  ResultantTask
//
//  Created by Sher Locked on 26.06.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation

class CurrencyListVMF: CurrencyListVMFProtocol {
    
    func createViewModels(with currency: [Currency]) -> [CurrencyCellViewModel] {
        let numberFormatter = NumberFormatter()
        numberFormatter.maximumFractionDigits = 2
        numberFormatter.minimumIntegerDigits = 1
        numberFormatter.minimumFractionDigits = 2
        let viewModels = currency.map { currency -> CurrencyCellViewModel in
            let volume = String(currency.volume)
            let amount = numberFormatter.string(from: currency.amount) ?? ""
            return CurrencyCellViewModel(name: currency.name, volume: volume, amount: amount)
        }
        
        return viewModels
    }
    
}
