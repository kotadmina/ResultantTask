//
//  CurrencyCellViewModel.swift
//  ResultantTask
//
//  Created by Sher Locked on 26.06.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

struct CurrencyCellViewModel {
    
    var name: String
    var volume: String
    var amount: String
    
}
