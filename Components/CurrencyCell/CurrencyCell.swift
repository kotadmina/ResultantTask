//
//  CurrencyCell.swift
//  ResultantTask
//
//  Created by Sher Locked on 26.06.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import UIKit

class CurrencyCell: UITableViewCell {
    
    static let cellHeight: CGFloat = 50
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var volumeLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    
    func configure(with viewModel: CurrencyCellViewModel) {
        nameLabel.text = viewModel.name
        volumeLabel.text = viewModel.volume
        amountLabel.text = viewModel.amount
    }
    
}
