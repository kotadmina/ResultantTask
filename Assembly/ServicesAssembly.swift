//
//  ServicesAssembly.swift
//  ResultantTask
//
//  Created by Sher Locked on 26.06.2018.
//  Copyright © 2018 Sher Locked. All rights reserved.
//

import Foundation
import Swinject

class ServicesAssembly: BaseAssembly {
    
    static func configure() {
        defaultContainer().register(NetworkServiceProtocol.self) { _ in
            return NetworkService()
            }.inObjectScope(.container)
    }
}
